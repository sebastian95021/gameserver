var io = require('socket.io')(8001)

console.log('server has started');

var players = [];
var rooms = [];
var sockets = [];
var queue = [];
var minPlayers = 2;

io.on('connection', function(socket){
    console.log('connection made');

    //Buscar indice disponible en el array de conexiones para darle un id al player
    player_id = getIndexAvailable(sockets);

    //Sino se encuentra indice disponible se le entrega una nueva posicion del array
    if (player_id == -1){
        sockets.push(socket);
        player_id = sockets.length-1;
    }else{
        sockets[player_id] = socket;
    }

    console.log(player_id);

    //Enviar al cliente su id de conexion
    socket.emit('playerId', {player_id});

    // Peticion de entrar a un jugador en cola o enviarlo a una partida
    socket.on('playerQueue', function(data){
        player_id = data.id;

        //Poner jugador en la cola
        queue.push(player_id);

        //consecutivo de la cola
        noplayer = queue.length;

        //Si ya hay 4 jugadores en la cola, enviarlos a una partida
        playersQueue = queue.length;
        if (playersQueue == minPlayers){
            room_id = getIndexAvailable(rooms);

            //Sino se encuentra indice disponible se le entrega una nueva posicion del array
            if (room_id == -1){
                rooms.push(queue.toString());
                room_id = rooms.length-1;
            }else{
                rooms[room_id] = queue.toString();
            }

            //Enviar al cliente su id de conexion
            socket.emit('NoPlayer', {noplayer});
            socket.broadcast.emit('startMatch', {room_id, allPlayersRoom: rooms[room_id]});
            socket.emit('startMatch', {room_id, allPlayersRoom: rooms[room_id]});

            //Limpiar cola
            queue = [];
        }
        else{
            socket.emit('NoPlayer', {noplayer});
        }

        console.log(queue);
        console.log(rooms);

    });



    socket.on('playerPosition', function(data){
        console.log(data);
        socket.broadcast.emit('OtherPlayersPosition', data);
    });


    //Evento de desconexion
    socket.on('disconnect', function(){
        player_id = sockets.indexOf(socket);
        delete sockets[player_id];
        console.log('A player has disconnected player_id:', player_id);
    });
});

//Obtener indice disponible de un array
function getIndexAvailable(obj){
    index = -1;
    for(var i = 0, len = obj.length; i < len; i++) {
        if (obj[i] == undefined){
            index = i;
            console.log('Indice disponible encontrado:', index);
            break;
        }
    }

    return index;
}